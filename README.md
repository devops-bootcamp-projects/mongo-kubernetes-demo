# Mongo Kubernetes Demo
* The folling files in this repo were used in a demonstration on deploying MongoDB and MongoExpress to a Kubernetes cluster (Minikube)
## Mongo DB Deployment
* mongo-deployment.yaml
  - includes MongoDB deployment and MongoDB internal service configurations
* mongo-secret.yaml
  - MongoDB credentials
* mongo-configmap.yaml
  - MongoDB config for the database server URL
* mongo-express-deployment.yaml
  - includes MongoExpress deployment and MongoExpress external service configurations
